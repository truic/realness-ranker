#!/usr/bin/env bash
BASEDIR=$(dirname "$0");
sudo docker build --no-cache -t realness_ranker $BASEDIR;
docker run --rm -d -p 5000:5000 --name realness_ranker realness_ranker;