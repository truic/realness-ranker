from realness_ranker import RealnessRanker

from flask import Flask, request, abort
import numpy as np
import requests
import json
import os

app = Flask(__name__)

model_path = os.path.join("models", "model_usa10k_random_name_10k.pkl")
ranker = RealnessRanker(model_path)

@app.route('/rank_names', methods = ['POST'])
def generate_names():
    try: #try to load post data
        message = json.loads(request.data)
    except json.JSONDecodeError:
        print(e)
        return abort(400)

    try:
        realness_ranking = ranker.predict(message["names"]).tolist()
    except Exception as e:
        print(f"An error occurred while trying to generate names: {e}") 
        return abort(500)

    if message.get("debug", 0):
        sorted_indices = np.argsort(realness_ranking)[::-1]
        names_with_ranking = [ {"Name" : message["names"][i] , "Score" : realness_ranking[i] } for i in sorted_indices ]
    else: 
        names_with_ranking = []

    return_message = {   
        "ranked" : realness_ranking, 
        "names" : names_with_ranking
    }

    return return_message

if __name__ == '__main__':
    app.run(host="0.0.0.0")