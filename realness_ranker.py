import os
import numpy as np
import pickle
import spacy
from xgboost import XGBClassifier

class RealnessRanker:
    def __init__(self, model_file):
        self.nlp = spacy.load("en_core_web_sm") #python -m spacy download en_core_web_sm
        self.model = pickle.load(open(model_file, "rb"))

    def string_to_vec(self, string):
        return self.nlp(string).vector

    def predict(self, name_arr):
        x = np.array([self.string_to_vec(name) for name in name_arr])
        y_prob = self.model.predict_proba(x)
        y_prob_true = y_prob[:,1]
        return y_prob_true

def main():
    test_arr = [
        "Biomed Ltd Escapes",
        "The Biomed Ltd",
        "Biomed Ltdly",
        "Lifelong Biomed Ltd",
        "Co Biomed Ltd",
        "Biomed Ltdism",
        "Biomed Ltd Garden",
        "Re Biomed Ltd",
        "Biomed Ltdfy",
        "Revitalizing Biomed Ltd",
        "My Biomed Ltd",
        "Biomed Ltdous",
        "Joyful Biomed Ltd",
        "We Biomed Ltd",
        "In Biomed Ltd",
        "Choice Biomed Ltd",
        "Biomed Ltd Up",
        "Biomed Ltdcy",
        "Balanced Biomed Ltd",
        "Go Biomed Ltd",
        "Biomed Escapes",
        "Lifelong Biomed",
        "Revitalizing Ltd",
        "Co Biomed",
        "Ltd Hop",
        "Ltd Age",
        "Biomedism",
        "Revitalizing Biomed",
        "Joyful Biomed",
        "Inclusive Ltd",
        "Biomed Age",
        "Ltd Ace",
        "Ltd Air",
        "Biomedfy",
        "Five Star Biomed",
        "Biomed Happiness",
        "Heavenly Ltd",
        "Biomed Sign",
        "Ltd Ster",
        "Ltd State",
        "The Divine Goddess Llc",
        "Co Divine Goddess Llc",
        "Re Divine Goddess Llc",
        "My Divine Goddess Llc",
        "We Divine Goddess Llc",
        "Divine Goddess Llc Up",
        "Divine Goddess Llc Me",
        "Divine Goddess Llc Co",
        "Divine Goddess Llc Io",
        "Pro Divine Goddess Llc",
        "Divine Goddess Llc Ice",
        "Divine Goddess Llc Hop",
        "Divine Goddess Llc Age",
        "Divine Goddess Llc Art",
        "On Divine Goddess Llc",
        "Un Divine Goddess Llc",
        "Divine Goddess Llc Inc",
        "Ex Divine Goddess Llc",
        "You Divine Goddess Llc",
        "New Divine Goddess Llc",
        "Divine Explored",
    ]

    ranker = RealnessRanker(os.path.join("models", "model_usa10k_random_name_10k.pkl"))
    realness_ranking = ranker.predict(test_arr)

    sorted_indices = np.argsort(realness_ranking)[::-1]

    for i in range(len(realness_ranking)):
        print(f"'{test_arr[i]}' score: {realness_ranking[i]}")
    # for i in sorted_indices:
    #    print(f"'{test_arr[i]}' score: {realness_ranking[i]}")
        
if __name__ == "__main__":
    main()